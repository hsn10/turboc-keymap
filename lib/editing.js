'use babel';

/** A word is defined as a sequence of characters separated by one of the following: */
const WORD_SEPARATORS = " <>,;.(){}^'*+-/$#_=|~?!\"%&:@\\";

export function InsertLine(event) {
   const editor = atom.workspace.getActiveTextEditor();
   if (!editor) {
         return;
   }
   const pos = editor.getCursorScreenPosition();
   editor.transact( function() {
         editor.insertNewline();
         editor.setCursorScreenPosition(pos, { "autoscroll" : false});
      }
   );
};

export function DeleteWordRight(event) {
   const editor = atom.workspace.getActiveTextEditor();
   if (!editor) {
         return;
   }
   editor.transact( function() {
         editor.deleteToNextWordBoundary();
         /* If we are at end of line, we do not need to call second delete */
         const pos = editor.getCursorBufferPosition();
         const line = editor.lineTextForBufferRow(pos.row);
         // console.log("col "+pos.column+",line len "+ line.length);
         if(pos.column < line.length) {
            editor.deleteToNextWordBoundary();
         }
      }
   );
};
