'use babel';

import { Point } from 'atom';
/**
 * Scroll editor window up. Cursor will stay at the same
 * place in the text, but won't go outside of visible area.
*/
export function WindowScrollUp(event) {
   const editor = atom.workspace.getActiveTextEditor();
   if (!editor.element) {
            return;
   }
   const editorElement = editor.element;
   editorElement.setScrollTop(editorElement.getScrollTop() - editor.getLineHeightInPixels());
   const pos = editor.getCursorScreenPosition();
   if ( pos.row > editorElement.getLastVisibleScreenRow() ) {
      editor.setCursorScreenPosition(new Point(pos.row-1, pos.column), { "autoscroll" : false});
   }
};

/**
 * Scroll editor window down. Cursor will stay at the same
 * place in the text, but won't go outside of visible area.
*/
export function WindowScrollDown(event) {
   const editor = atom.workspace.getActiveTextEditor();
   if (!editor.element) {
            return;
   }
   const editorElement = editor.element;
   editorElement.setScrollTop(editorElement.getScrollTop() + editor.getLineHeightInPixels());
   const pos = editor.getCursorScreenPosition();
   if ( pos.row < editorElement.getFirstVisibleScreenRow() ) {
      editor.setCursorScreenPosition(new Point(pos.row+1, pos.column), { "autoscroll" : false});
   }
};

/**
 * Go to window top, keep column
 */
export function WindowTop(event) {
   const editor = atom.workspace.getActiveTextEditor();
   if (!editor.element) {
            return;
   }
   const editorElement = editor.element;
   const pos = editor.getCursorScreenPosition();
   editor.setCursorScreenPosition(new Point(editorElement.getFirstVisibleScreenRow(), pos.column), { "autoscroll" : false});
}

/**
 * Go to window bottom, keep column
 */
export function WindowBottom(event) {
   const editor = atom.workspace.getActiveTextEditor();
   if (!editor.element) {
            return;
   }
   const editorElement = editor.element;
   const pos = editor.getCursorScreenPosition();
   editor.setCursorScreenPosition(new Point(editorElement.getLastVisibleScreenRow(), pos.column), { "autoscroll" : false});
}
