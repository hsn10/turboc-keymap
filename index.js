'use babel';

import { CompositeDisposable } from 'atom';
import { WindowScrollUp, WindowScrollDown, WindowTop, WindowBottom } from './lib/window.js';
import { InsertLine, DeleteWordRight } from './lib/editing.js';

export default {

   subscriptions: null,

   activate(state) {
      this.subscriptions = new CompositeDisposable();

      // Register command that toggles this view
      this.subscriptions.add(
         atom.commands.add('atom-workspace atom-text-editor',
                         { "borland-cpp:window-scroll-up" : WindowScrollUp,
                           "borland-cpp:window-scroll-down" : WindowScrollDown,
                           "borland-cpp:window-top" : WindowTop,
                           "borland-cpp:window-bottom" : WindowBottom,
                           "borland-cpp:insert-line" : InsertLine,
                           "borland-cpp:delete-word-right" : DeleteWordRight
                       }
      ));
   },

   deactivate() {
      this.subscriptions.dispose();
   }
};
